// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunctionalComponents/SchemeState.h"
#include "BoardInteractState.generated.h"

/**
 * 
 */
class ASchemeActor;

UCLASS()
class HYDROSCHEMES_API UBoardInteractState : public USchemeState
{
	GENERATED_BODY()
public:
	virtual void Interact(UObject* StateOwner = nullptr) override;
	void ShowSchemeOutputHint(ASchemeActor* Owner);
};
