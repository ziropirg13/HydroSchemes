// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunctionalComponents/SchemeState.h"
#include "PickupInteractState.generated.h"

class IPickUpInterface;

/**
 * 
 */
UCLASS()
class HYDROSCHEMES_API UPickupInteractState : public USchemeState
{
	GENERATED_BODY()
public:
	virtual void Interact(UObject* StateOwner = nullptr) override;
private:
	void Pickup(IPickUpInterface* StateOwner);
};
