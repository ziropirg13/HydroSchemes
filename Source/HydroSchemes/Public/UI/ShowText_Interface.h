// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ShowText_Interface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UShowText_Interface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class HYDROSCHEMES_API IShowText_Interface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
};
