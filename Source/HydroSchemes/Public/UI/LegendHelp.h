// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "LegendHelp.generated.h"

class UHorizontalBox;

UCLASS()
class HYDROSCHEMES_API ULegendHelp : public UUserWidget
{
    GENERATED_BODY()

public:
    UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
    UImage* BaseLegend;
    UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
    UHorizontalBox* HintBox;
    void AddButtonToHintBox();
};