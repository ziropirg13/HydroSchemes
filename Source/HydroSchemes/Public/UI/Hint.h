// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Hint.generated.h"

class UBorder;//���������� ������ �������
class UTextBlock;//���������� ������ �������
class UImage;//���������� ����������� �������
/**
 * ������� ���������� ����, ���������� �� ������ ������ ��� ���������
 * ������� ���������� ����, ���������� �� ������ ����������� ��� ���������
 */


UCLASS()
class HYDROSCHEMES_API UHint : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* HintInputSocket;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UBorder* HintInputSocketBorder;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* HintText;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UImage* HintImage;

	UPROPERTY(EditDefaultsOnly)
	FName ImageMaterialParameter;

	void SetHintHeaderText(FText InputText);
	void SetHintSocketText(FText InputText);
	void SetImageIndex(float ImageIndex);
};
//������� �����, �����������, ���������� � ���������