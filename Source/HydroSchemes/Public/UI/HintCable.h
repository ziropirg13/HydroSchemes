// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/Hint.h"
#include "HintCable.generated.h"

/**
 * 
 */
UCLASS()
class HYDROSCHEMES_API UHintCable : public UHint
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* HintInputSocketLink;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UBorder* HintInputSocketBorderLink;
	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UTextBlock* HintTextLink;

	UPROPERTY(BlueprintReadOnly, meta = (BindWidget))
	UImage* HintImageLink;

	UPROPERTY(EditDefaultsOnly)
	FName ImageMaterialParameterLink;


	void SetHintHeaderTextLink(FText InputText);
	void SetHintSocketTextLink(FText InputText);
	void SetImageIndexLink(float ImageIndex);
};
