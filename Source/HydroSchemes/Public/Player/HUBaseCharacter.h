// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Inventory/InventoryElement.h"
#include "InputActionValue.h"
#include "HUBaseCharacter.generated.h"

#define INTERACT ECC_GameTraceChannel1

struct FInventoryElement;
struct FBoardActorOutput;
class UInputMappingContext;
class UInputAction;
class UCameraComponent;
class UInventoryComponent;
class ABoardSchemeActor;
class ABoardPart;
class UInventory;
class UMiniMainMenu;
class UMiniSettingsMenu;
class UHoloState;
class ULegendHelp;
class UInputModeState;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUIStateSet);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameAndUIStateSet);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHoverHit, FHitResult);

UCLASS()
class HYDROSCHEMES_API AHUBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AHUBaseCharacter();

protected:
	FHitResult LineTraceSingle(const FVector& StartLoaction,const FVector& EndLocation);

	UFUNCTION(BlueprintImplementableEvent, Category = "InteractableHUD")
	void OnHeldActorInSight(const FText& Text);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	TObjectPtr<UCameraComponent> CameraComponent;

	UPROPERTY(EditDefaultsOnly, Category = "UIWidget")
	TSubclassOf<UInventory> InventoryWidgetClass;
	UPROPERTY(VisibleAnywhere, Category = "UIWidget")
	TObjectPtr<UInventory> InventoryWidget;

	UPROPERTY(EditDefaultsOnly,Category = "UIWidget")
	TSubclassOf<UMiniMainMenu> MiniMainMenuWidgetClass;
	UPROPERTY(VisibleAnywhere, Category = "UIWidget")
	TObjectPtr<UMiniMainMenu> MiniMainMenuWidget;

	UPROPERTY(EditDefaultsOnly, Category = "UIWidget")
	TSubclassOf<UMiniSettingsMenu> MiniSettingsMenuWidgetClass;
	UPROPERTY(VisibleAnywhere, Category = "UIWidget")
	TObjectPtr<UMiniSettingsMenu> MiniSettingsMenuWidget;

	UPROPERTY(EditDefaultsOnly, Category = "UIWidget")
	TSubclassOf<ULegendHelp> LegendHelpClass;
	UPROPERTY(VisibleAnywhere, Category = "UIWidget")
	TObjectPtr<ULegendHelp> LegendHelpWidget;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UPROPERTY()
	ASchemeActor* OldHitActor = nullptr;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	TObjectPtr<UInventoryComponent> InventoryComponent;
	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	FName PlacingElementAttachSocketName = "HSItemSocket";

	UPROPERTY()
	TObjectPtr<ASchemeActor> PlacingElement = nullptr;

	UPROPERTY(EditAnywhere)
	TObjectPtr<UMaterialInterface> HoloMaterial;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputMappingContext> PlayerInputs;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> MovementAction;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> LookAction;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> InteractAction;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> ToggleInventoryAction;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> ToggleMiniMainMenuAction;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> ToggleLegendHelpAction;

	UPROPERTY(EditAnywhere, Category = "Input")
	TObjectPtr<UInputAction> DeleteHoloAction;

	UPROPERTY(EditAnywhere, Category = "Holo")
	FColor CorrectHoloColor;

	UPROPERTY(EditAnywhere, Category = "Holo")
	FColor IncorrectHoloColor;
	UPROPERTY()
	TObjectPtr<ASchemeActor> Holo;

	FOnHoverHit OnHoverHit;
	
	FOnUIStateSet OnUIStateSet;
	FOnGameAndUIStateSet OnGameAndUIStateSet;
	UFUNCTION(BlueprintCallable)
	void SpawnActorInHand(const FInventoryElement& InventoryElement);

	UFUNCTION()
	void Interact();

	UFUNCTION()
	void ShowHolo();

	void CheckHoloState(const ABoardPart* AttachBoardPart);
	void CheckHoloState(ABoardSchemeActor* AttachBoardSchemeActor, const FName SocketOutputName);
	void DestroyHolo();
	void AddItemToInventory(FInventoryElement& SchemeActor);

	UFUNCTION()
	void ToggleMiniMainMenu();

	UFUNCTION()
	void ToggleMiniSettingsMenu();
	void PlaceElement();
	void CreateUI();

	// States
	void SetGameAndUIState();
	void SetUIState();
	UPROPERTY()
	TObjectPtr<UHoloState> HoloState;
	void SetDeletedHoloState();
	void SetCorrectHoloState();
	void SetIncorrectHoloState();
	UFUNCTION()
	void ToggleLegendHelp();

	//UFUNCTION()
	//void TraseActorText();
	UPROPERTY(VisibleAnywhere)
	TObjectPtr<UInputModeState> CharacterState;

	UFUNCTION()
	void ShowHint(); // ���������� ������� ��� ������ ���������
private:

	UFUNCTION()
	void Move(const FInputActionValue& Value);
	UFUNCTION()
	void Look(const FInputActionValue& Value);
	UFUNCTION()
	void ToggleInventory();

	UFUNCTION()
	void DeleteHolo();
};
