// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalComponents/HoloState.h"
#include "CorrectHoloState.generated.h"

/**
 * 
 */
UCLASS()
class HYDROSCHEMES_API UCorrectHoloState : public UHoloState
{
	GENERATED_BODY()
public:
	virtual void PlaceElement() override;
	virtual void Interact() override;
	virtual void DeleteHolo() override;
};
