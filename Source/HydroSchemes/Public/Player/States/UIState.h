// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FunctionalComponents/InputModeState.h"
#include "UIState.generated.h"

/**
 * 
 */
UCLASS()
class HYDROSCHEMES_API UUIState : public UInputModeState
{
	GENERATED_BODY()
};
