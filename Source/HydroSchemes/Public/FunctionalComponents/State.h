// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "VisualLogger/VisualLogger.h"
#include "State.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class HYDROSCHEMES_API UState : public UObject
{
	GENERATED_BODY()
public:
    virtual void SetFlags(EObjectFlags NewFlags)
    {
        UE_VLOG(this, LogTemp, Log, TEXT("Setting flags: %d"), NewFlags);
        Super::SetFlags(NewFlags);
    }

    virtual void ClearFlags(EObjectFlags FlagsToClear)
    {
        UE_VLOG(this, LogTemp, Log, TEXT("Clearing flags: %d"), FlagsToClear);
        Super::ClearFlags(FlagsToClear);
    }
};
