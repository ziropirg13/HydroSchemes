// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HintComponent.generated.h"



class UHint;

DECLARE_MULTICAST_DELEGATE(FOnBeginSocketViewOverlap);
DECLARE_MULTICAST_DELEGATE(FOnEndSocketViewOverlap);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HYDROSCHEMES_API UHintComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHintComponent();

	UPROPERTY(EditDefaultsOnly, Category = "UIWidget")
	TSubclassOf<UHint> HintWidgetClass;
	UPROPERTY(VisibleAnywhere, Category = "UIWidget")
	TObjectPtr<UHint> HintWidget;


	FOnBeginSocketViewOverlap OnBeginSocketViewOverlap;
	FOnEndSocketViewOverlap OnEndSocketViewOverlap;
protected:
	// Called when the game starts
	//virtual void BeginPlay() override;

public:	
	void Init();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void CreateHintWidget();
	void OpenHintWidget();
	void CloseHintWidget();
	void ShowHintSocketWidget(FName SocketName);
	void HideHintocketWidget();


};
