// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "FunctionalComponents/State.h"
#include "InputModeState.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class HYDROSCHEMES_API UInputModeState : public UState
{
	GENERATED_BODY()

public:
	virtual void Interact() {};
	virtual void DeleteHolo() {};
	virtual void ShowHint() {};
};
