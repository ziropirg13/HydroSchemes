// HydroSchemes Simulator. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SchemeOutputHintComponent.generated.h"

class USchemeOutputHint;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HYDROSCHEMES_API USchemeOutputHintComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USchemeOutputHintComponent();

	UPROPERTY(EditDefaultsOnly, Category = "UIWidget")
	TSubclassOf<USchemeOutputHint> SchemeOutputHintWidgetClass;
	UPROPERTY(VisibleAnywhere, Category = "UIWidget")
	TObjectPtr<USchemeOutputHint> SchemeOutputHintWidget;
protected:
	// Called when the game starts
	//virtual void BeginPlay() override;

public:	
	void Init();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	void CreateSchemeOutputHintWidget();
	void OpenSchemeOutputHintWidget();
	void CloseSchemeOutputHintWidget();
		
};