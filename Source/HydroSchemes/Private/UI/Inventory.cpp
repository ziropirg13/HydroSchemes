// HydroSchemes Simulator. All rights reserved.


#include "UI/Inventory.h"
#include "Components/VerticalBox.h"
#include "Kismet/GameplayStatics.h"
#include "Player/HUPlayerController.h"
#include "UI/InventorySlot.h"
#include "Engine/Engine.h"
#include "Player/HUBaseCharacter.h"
#include "Inventory/InventoryComponent.h"

void UInventory::AddItemToList(const FInventoryElement& InventoryElement)
{
	if (!GetWorld()) return;
	AHUPlayerController* PlayerController = Cast<AHUPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (PlayerController && InventorySlotClass) {
		auto InventorySlot = CreateWidget<UInventorySlot>(PlayerController, InventorySlotClass);
		if (ItemsList) {
			ItemsList->AddChild(InventorySlot);
			InventorySlot->SetInventoryItem(InventoryElement);
		}
	}
}

void UInventory::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	if (!GetWorld()) return;
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	Character->InventoryComponent->OnInventoryItemAdded.AddDynamic(this, &UInventory::AddItemToList);
}
