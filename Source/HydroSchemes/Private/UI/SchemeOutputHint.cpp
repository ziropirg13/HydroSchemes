// HydroSchemes Simulator. All rights reserved.


#include "UI/SchemeOutputHint.h"
#include "Components/Image.h"

void USchemeOutputHint::SetImageIndex(float ImageIndex)
{
	HintImage->GetDynamicMaterial()->SetScalarParameterValue(ImageMaterialParameter, ImageIndex);
}
