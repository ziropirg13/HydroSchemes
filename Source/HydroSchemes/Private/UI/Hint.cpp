// HydroSchemes Simulator. All rights reserved.


#include "UI/Hint.h"
#include "Components/TextBlock.h"
#include "Components/Border.h"
#include "Components/Image.h"

void UHint::SetHintHeaderText(FText InputText)
{
	HintText->SetText(InputText);
}

void UHint::SetHintSocketText(FText InputText)
{
	HintInputSocket->SetText(InputText);
}

void UHint::SetImageIndex(float ImageIndex)
{
	HintImage->GetDynamicMaterial()->SetScalarParameterValue(ImageMaterialParameter, ImageIndex);
}
