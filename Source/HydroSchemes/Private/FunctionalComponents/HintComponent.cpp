// HydroSchemes Simulator. All rights reserved.


#include "FunctionalComponents/HintComponent.h"
#include "Kismet/GameplayStatics.h"
#include "UI/Hint.h"
#include "HSActors/SchemeActor.h"
#include "Components/TextBlock.h"
#include "Components/Border.h"
#include "HSActors/BoardSchemeActor.h"
#include "Player\HUBaseCharacter.h"
// Sets default values for this component's properties
UHintComponent::UHintComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


//// Called when the game starts
//void UHintComponent::BeginPlay()
//{
//	Super::BeginPlay();
//
//	// ...
//	
//}


//ABoardSchemeActor* BoardSchemeActor = Cast<ABoardSchemeActor>(GetOwner());
//
//FName NearestSocketOutput = BoardSchemeActor->GetNearestSocketName(HitResult)
//HintWidget->SetHintSocketText(FText::FromName(NearestSocketOutput));

void UHintComponent::Init()
{

	CreateHintWidget();
	ASchemeActor* SchemeActor = Cast<ASchemeActor>(GetOwner());
	HintWidget->SetHintHeaderText(FText::FromName(SchemeActor->ActorName));
	HintWidget->SetImageIndex(SchemeActor->HintImageIndex);


	/*� �������� ������� inventory component
* ��� ��� ������������ ������
* ��� ���������� ������ �� ����� � ��� ��������� ������ � ������
* ������� �������������. �� ������ - �������� ������ �� ShemeActor.
* GetOwner() - ���������� ���������. ������� Cast � shemeActor
*
*/
}

// Called every frame
void UHintComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHintComponent::CreateHintWidget()
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (!PlayerController) return;
	HintWidget = CreateWidget<UHint>(PlayerController, HintWidgetClass);
	HintWidget->AddToViewport(1);
	HintWidget->SetVisibility(ESlateVisibility::Hidden);
}

void UHintComponent::OpenHintWidget()
{
	
	HintWidget->SetVisibility(ESlateVisibility::Visible);
}

void UHintComponent::CloseHintWidget()
{


	HintWidget->SetVisibility(ESlateVisibility::Hidden);
}

void UHintComponent::ShowHintSocketWidget(FName SocketName)
{
	if (SocketName.IsNone()) return;
	HintWidget->SetHintSocketText(FText::FromString(SocketName.ToString())); 
	UE_LOG(LogTemp, Warning, TEXT("HintSocket - %s"), *HintWidget->HintInputSocket->GetText().ToString());
	HintWidget->HintInputSocket->SetVisibility(ESlateVisibility::Visible);
	HintWidget->HintInputSocketBorder->SetBrushColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
}

void UHintComponent::HideHintocketWidget()
{
	HintWidget->HintInputSocket->SetVisibility(ESlateVisibility::Collapsed);
	HintWidget->HintInputSocketBorder->SetBrushColor(FLinearColor(1.0f, 1.0f, 1.0f, 0.0f));
}

