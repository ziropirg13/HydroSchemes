// HydroSchemes Simulator. All rights reserved.


#include "Player/States/IncorrectHoloState.h"
#include "Kismet/GameplayStatics.h"
#include "Player/HUBaseCharacter.h"

void UIncorrectHoloState::DeleteHolo()
{
	if (!GetWorld()) return;
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	Character->DestroyHolo();
	if (Character->PlacingElement && !Character->PlacingElement->IsActorBeingDestroyed()) {
		Character->PlacingElement->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Character->PlacingElement->Destroy();
	}
	Character->SetDeletedHoloState();
}
