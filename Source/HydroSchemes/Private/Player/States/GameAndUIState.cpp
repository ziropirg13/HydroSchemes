// HydroSchemes Simulator. All rights reserved.


#include "Player/States/GameAndUIState.h"
#include "Player/HUBaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Player/States/DeletedHoloState.h"

void UGameAndUIState::Interact()
{
	if (!GetWorld()) return;
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	Character->HoloState->Interact();
}

void UGameAndUIState::DeleteHolo()
{
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	Character->HoloState->DeleteHolo();
}

void UGameAndUIState::ShowHint()	/* ���������� ������� ������ ���������*/
{
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character || !Character->HoloState) return;
	Character->HoloState->ShowHint();
}
