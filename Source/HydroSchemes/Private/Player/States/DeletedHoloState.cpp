// HydroSchemes Simulator. All rights reserved.


#include "Player/States/DeletedHoloState.h"
#include "Player/HUBaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Utils/HoverUtils.h"

void UDeletedHoloState::Interact()
{
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;

	FVector ViewLocation;
	FRotator ViewRotation;

	Character->Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);

	FVector TraceStart = ViewLocation;
	FVector TraceEnd = TraceStart + ViewRotation.Vector() * 1500.0f;

	FHitResult HitResult;

	if (GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, INTERACT)) {
		if (!HitResult.bBlockingHit) return;
		auto HitActor = HitResult.GetActor();
		auto HitComponent = HitResult.GetComponent();
		if (HitActor && HitActor->Implements<UInteractableInterface>()) {
			IInteractableInterface* InteractActor = Cast<IInteractableInterface>(HitActor);
			InteractActor->Interact();
		}
		if (HitComponent && HitComponent->Implements<UInteractableInterface>()) {
			IInteractableInterface* InteractComponent = Cast<IInteractableInterface>(HitComponent);
			InteractComponent->Interact();
		}
	}
}

void UDeletedHoloState::ShowHint()
{
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;

	HoverUtils::Hover<ASchemeActor>(Character->OldHitActor, GetWorld());
}
