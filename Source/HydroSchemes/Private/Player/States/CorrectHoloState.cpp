// HydroSchemes Simulator. All rights reserved.


#include "Player/States/CorrectHoloState.h"
#include "Player/HUBaseCharacter.h"
#include "Kismet/GameplayStatics.h"

void UCorrectHoloState::PlaceElement()
{
	if (!GetWorld()) return;
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	if (Character->Holo) {
		Character->Holo->PlaceElement();
	}
}

void UCorrectHoloState::Interact()
{
	PlaceElement();
}

void UCorrectHoloState::DeleteHolo()
{
	if (!GetWorld()) return;
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	Character->DestroyHolo();
	if (Character->PlacingElement && !Character->PlacingElement->IsActorBeingDestroyed()) {
		Character->PlacingElement->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Character->PlacingElement->Destroy();
	}
	Character->SetDeletedHoloState();
}
