// HydroSchemes Simulator. All rights reserved.


#include "Player/HUBaseCharacter.h"
#include "Blueprint/UserWidget.h"
#include "Camera/CameraComponent.h"
#include "HSActors/Board.h"
#include "HSActors/BoardPart.h"
#include "HSActors/Plus.h"
#include "HSActors/SchemeActor.h"
#include "Interfaces/InteractableInterface.h"
#include "Interfaces/PickUpInterface.h"
#include "Inventory/InventoryComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Player/HUPlayerController.h"
#include "UI/Inventory.h"
#include "Components/InputComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "HSActors/BoardSchemeActor.h"
#include "HSActors/Cable.h"
#include "UI/MiniMainMenu.h"
#include "UI/MiniSettingsMenu.h"
#include "UI/LegendHelp.h"
#include "UI/ShowText_Interface.h"
#include "Player/States/GameAndUIState.h"
#include "Player/States/UIState.h"
#include "Player/States/DeletedHoloState.h"
#include "Player/States/CorrectHoloState.h"
#include "Player/States/IncorrectHoloState.h"


DEFINE_LOG_CATEGORY_STATIC(BaseCharacterLog, All, All)



// Sets default values
AHUBaseCharacter::AHUBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(GetMesh(), "head");
	CameraComponent->bUsePawnControlRotation = true;
	InventoryComponent = CreateDefaultSubobject<UInventoryComponent>("InventoryComponent");	
}

FHitResult AHUBaseCharacter::LineTraceSingle(const FVector& StartLoaction, const FVector& EndLocation)
{
	FHitResult HitResult;

	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);

	GetWorld()->LineTraceSingleByChannel(HitResult, StartLoaction, EndLocation, INTERACT, Params);

	return HitResult;
}

// Called when the game starts or when spawned
void AHUBaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	//���������� ��������� � ���������
	SetGameAndUIState();
	SetDeletedHoloState();
	//�������� �������� ���������������� ��������
	APlayerController* PlayerController = Cast<APlayerController>(Controller);
	check(PlayerController);
	UEnhancedInputLocalPlayerSubsystem* Subsystem = PlayerController->GetLocalPlayer()->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>();
	check(Subsystem);
	Subsystem->AddMappingContext(PlayerInputs, 0);

	CreateUI();

	// �������� ������� ToggleLegendHelp ��� ������ ����
	//ToggleLegendHelp();

	//������������ ������� ������ ��������� �� ����� ��� ��������� �� �������
	//OnHeldActorInSight(FText::FromString("TestingText"));


}

// Called every frame
void AHUBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ShowHolo();
	//����� ������� ����������� ��������� 
	ShowHint();
}

// Called to bind functionality to input
void AHUBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) 
	{
		check(MovementAction);
		EnhancedInputComponent->BindAction(MovementAction, ETriggerEvent::Triggered, this, &AHUBaseCharacter::Move);
		check(LookAction);
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &AHUBaseCharacter::Look);
		check(InteractAction);
		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this, &AHUBaseCharacter::Interact);
		check(ToggleInventoryAction);
		EnhancedInputComponent->BindAction(ToggleInventoryAction, ETriggerEvent::Triggered, this, &AHUBaseCharacter::ToggleInventory);
		check(DeleteHoloAction);
		EnhancedInputComponent->BindAction(DeleteHoloAction, ETriggerEvent::Triggered, this, &AHUBaseCharacter::DeleteHolo);
		check(ToggleMiniMainMenuAction);
		EnhancedInputComponent->BindAction(ToggleMiniMainMenuAction, ETriggerEvent::Triggered, this, &AHUBaseCharacter::ToggleMiniMainMenu);
		check(ToggleLegendHelpAction);
		EnhancedInputComponent->BindAction(ToggleLegendHelpAction, ETriggerEvent::Triggered, this, &AHUBaseCharacter::ToggleLegendHelp);
	}
}

void AHUBaseCharacter::Interact()
{
	check(CharacterState);
	CharacterState->Interact();
}

void AHUBaseCharacter::SpawnActorInHand(const FInventoryElement& InventoryElement)
{
	if (!InventoryElement.ElementClass || InventoryElement.ElementName == "") return;
	if (!GetWorld()) return;
	if (!PlacingElement && PlacingElement->StaticClass() == InventoryElement.ElementClass) return;
	if (PlacingElement) {
		PlacingElement->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		PlacingElement->Destroy();
	}
	if (!PlacingElement || PlacingElement->IsActorBeingDestroyed()) {
		PlacingElement = GetWorld()->SpawnActor<ASchemeActor>(InventoryElement.ElementClass);
		PlacingElement->SetResponseToChannel(ECC_Pawn, ECR_Ignore);
		PlacingElement->DisableCollision();
		PlacingElement->AttachToCharacter(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale, PlacingElementAttachSocketName);
	}
	if(Holo && !Holo->IsActorBeingDestroyed()) {
		DestroyHolo();
	}
}

void AHUBaseCharacter::ShowHolo()
{
	if (PlacingElement && !PlacingElement->IsActorBeingDestroyed()) {
		FVector ViewLocation;
		FRotator ViewRotation;

		Controller->GetPlayerViewPoint(ViewLocation, ViewRotation);

		FVector TraceStart = ViewLocation;
		FVector TraceEnd = TraceStart + ViewRotation.Vector() * 1500.0f;

		FHitResult HitResult;

		if (GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, PlacingElement->CollisionChannelToPlace)) {
			if (!HitResult.bBlockingHit) return;
			auto HitActor = HitResult.GetActor();
			if (HitActor->IsA(PlacingElement->ActorsToPlace)) {
				PlacingElement->ShowHolo(this, HitResult);
			}
		}
	}
}

void AHUBaseCharacter::DestroyHolo()
{
	if (Holo && !Holo->IsActorBeingDestroyed()) {
		Holo->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Holo->Destroy();
	}
}

void AHUBaseCharacter::AddItemToInventory(FInventoryElement& SchemeActor)
{
	InventoryComponent->AddItemToInventory(SchemeActor);
}

void AHUBaseCharacter::Move(const FInputActionValue& Value)
{
	const FVector2D MovementVector = Value.Get<FVector2D>();

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);

	const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(ForwardDirection, MovementVector.Y);
	const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	AddMovementInput(RightDirection, MovementVector.X);
}

void AHUBaseCharacter::Look(const FInputActionValue& Value)
{
	const FVector2D LookAxisVector = Value.Get<FVector2D>();

	AddControllerPitchInput(LookAxisVector.Y);
	AddControllerYawInput(LookAxisVector.X);
}

void AHUBaseCharacter::ToggleInventory()
{
	if (!InventoryComponent->IsInventoryWidgetValid()) return;
	if (InventoryComponent->InventoryWidget->IsVisible()) {
		InventoryComponent->CloseInventoryWidget();
	}
	else {
		InventoryComponent->OpenInventoryWidget();
	}
}

void AHUBaseCharacter::ToggleMiniMainMenu()
{
	APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (!PlayerController) return;
	if (MiniMainMenuWidget->IsInViewport()) {
		FInputModeGameOnly InputGameMode;
		PlayerController->SetInputMode(InputGameMode);
		PlayerController->SetShowMouseCursor(false);
		PlayerController->SetIgnoreLookInput(false);
		PlayerController->SetIgnoreMoveInput(false);
		MiniMainMenuWidget->RemoveFromParent();
		SetGameAndUIState();
	}
	else {
		SetUIState();
		MiniMainMenuWidget->AddToViewport(1);
		PlayerController->SetIgnoreLookInput(true);
		PlayerController->SetIgnoreMoveInput(true);
		FInputModeGameAndUI InputGameUIMode;
		InputGameUIMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
		InputGameUIMode.SetHideCursorDuringCapture(false);
		PlayerController->SetInputMode(InputGameUIMode);
		PlayerController->SetShowMouseCursor(true);
	}
}

void AHUBaseCharacter::ToggleMiniSettingsMenu()
{
	APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (!PlayerController) return;
	if (MiniSettingsMenuWidget->IsInViewport()) {
		FInputModeGameOnly InputGameMode;
		PlayerController->SetInputMode(InputGameMode);
		PlayerController->SetShowMouseCursor(false);
		PlayerController->SetIgnoreLookInput(false);
		PlayerController->SetIgnoreMoveInput(false);
		MiniSettingsMenuWidget->RemoveFromParent();
		SetGameAndUIState();
	}
	else {
		SetUIState();
		MiniSettingsMenuWidget->AddToViewport(1);
		PlayerController->SetIgnoreLookInput(true);
		PlayerController->SetIgnoreMoveInput(true);
		FInputModeGameAndUI InputGameUIMode;
		InputGameUIMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
		InputGameUIMode.SetHideCursorDuringCapture(false);
		PlayerController->SetInputMode(InputGameUIMode);
		PlayerController->SetShowMouseCursor(true);
	}
}

void AHUBaseCharacter::DeleteHolo()
{
	check(CharacterState);
	CharacterState->DeleteHolo();
}

void AHUBaseCharacter::ToggleLegendHelp()
{
	if (!LegendHelpWidget) {
		if (APlayerController* PlayerController = Cast<APlayerController>(Controller)) {
			LegendHelpWidget = CreateWidget<ULegendHelp>(PlayerController, LegendHelpClass);

			LegendHelpWidget->AddToViewport(1);
		}
	}
	else {
		if (APlayerController* PlayerController = Cast<APlayerController>(Controller)) {
			LegendHelpWidget->RemoveFromParent();
			LegendHelpWidget = nullptr;
		}
	}
}

void AHUBaseCharacter::PlaceElement()
{
	check(HoloState);
	HoloState->PlaceElement();
}

void AHUBaseCharacter::SetGameAndUIState()
{
	UGameAndUIState* State = NewObject<UGameAndUIState>(this);
	check(State);
	CharacterState = State;
	OnGameAndUIStateSet.Broadcast();
}

void AHUBaseCharacter::SetUIState()
{
	UUIState* State = NewObject<UUIState>(this);
	check(State);
	CharacterState = State;
	OnUIStateSet.Broadcast();
}

void AHUBaseCharacter::SetDeletedHoloState()
{
	TObjectPtr<UDeletedHoloState> DeletedHoloState = NewObject<UDeletedHoloState>(this);
	check(DeletedHoloState);
	HoloState = DeletedHoloState;
}

void AHUBaseCharacter::SetCorrectHoloState()
{
	TObjectPtr<UCorrectHoloState> CorrectHoloState = NewObject<UCorrectHoloState>(this);
	check(CorrectHoloState);
	HoloState = CorrectHoloState;
}

void AHUBaseCharacter::SetIncorrectHoloState()
{
	TObjectPtr<UIncorrectHoloState> IncorrectHoloState = NewObject<UIncorrectHoloState>(this);
	check(IncorrectHoloState);
	HoloState = IncorrectHoloState;
}

void AHUBaseCharacter::CreateUI()
{
	if (!GetWorld()) return;
	if (!(IsPlayerControlled() && InventoryComponent && InventoryComponent->InventoryWidgetClass)) return;
	InventoryComponent->CreateInventoryWidget();
	APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (!PlayerController) return;
	MiniMainMenuWidget = CreateWidget<UMiniMainMenu>(PlayerController, MiniMainMenuWidgetClass);
	MiniSettingsMenuWidget = CreateWidget<UMiniSettingsMenu>(PlayerController, MiniSettingsMenuWidgetClass);
}

void AHUBaseCharacter::CheckHoloState(const ABoardPart* AttachBoardPart)
{
	if (auto BoardHoloSchemeActor = Cast<ABoardSchemeActor>(Holo)) {
		auto Board = Cast<ABoard>(AttachBoardPart->GetAttachParentActor());
		if (Board) {
			BoardHoloSchemeActor->InitSize(Board->GetBoardPartSize().X);
			if (Board->CanPlaceHolo(BoardHoloSchemeActor)) {
				BoardHoloSchemeActor->SetHoloMaterialColor(CorrectHoloColor);
				SetCorrectHoloState();
			}
			else {
				BoardHoloSchemeActor->SetHoloMaterialColor(IncorrectHoloColor);
				SetIncorrectHoloState();
			}
		}
	}
}


void AHUBaseCharacter::ShowHint()
{
	CharacterState->ShowHint();
}
void AHUBaseCharacter::CheckHoloState(ABoardSchemeActor* AttachBoardSchemeActor, const FName SocketOutputName)
{
	if (auto CableHolo = Cast<ACable>(Holo)) {
		if (AttachBoardSchemeActor->CanPlaceHolo(SocketOutputName)) {
			if (CableHolo->bIsCableLeftFixed) {
				auto ParentActor = Cast<ABoardSchemeActor>(CableHolo->GetAttachParentActor());
				if (ParentActor && (ParentActor == AttachBoardSchemeActor) && (CableHolo->GetAttachParentSocketName() == SocketOutputName)) {
					CableHolo->SetHoloMaterialColor(IncorrectHoloColor);
					SetIncorrectHoloState();
					return;
				}
			}

			CableHolo->SetHoloMaterialColor(CorrectHoloColor);
			SetCorrectHoloState();
		}
		else {
			CableHolo->SetHoloMaterialColor(IncorrectHoloColor);
			SetIncorrectHoloState();
		}
	}
}