// HydroSchemes Simulator. All rights reserved.


#include "Inventory/InventoryComponent.h"
#include "HSActors/SchemeActor.h"
#include "Kismet/GameplayStatics.h"
#include "UI/Inventory.h"
#include "Player/HUBaseCharacter.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


void UInventoryComponent::AddItemToInventory(FInventoryElement& SchemeActor)
{
	Inventory.Add(SchemeActor);
	OnInventoryItemAdded.Broadcast(SchemeActor);
}

void UInventoryComponent::CreateInventoryWidget()
{
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (!PlayerController) return;
	InventoryWidget = CreateWidget<UInventory>(PlayerController, InventoryWidgetClass);
	InventoryWidget->AddToViewport(1);
	InventoryWidget->SetVisibility(ESlateVisibility::Hidden);
}

bool UInventoryComponent::IsInventoryWidgetValid()
{
	UE_LOG(LogTemp, Warning, TEXT("Press - %i"), InventoryWidget && InventoryWidget->IsValidLowLevel());
	return InventoryWidget && InventoryWidget->IsValidLowLevel();
}

void UInventoryComponent::OpenInventoryWidget()
{
	APlayerController* PlayerController = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!PlayerController || !Character) return;
	Character->SetUIState();
	InventoryWidget->SetVisibility(ESlateVisibility::Visible);
	PlayerController->SetIgnoreLookInput(true);
	PlayerController->SetIgnoreMoveInput(true);
	FInputModeGameAndUI InputGameUIMode;
	InputGameUIMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	InputGameUIMode.SetHideCursorDuringCapture(false);
	PlayerController->SetInputMode(InputGameUIMode);
	PlayerController->SetShowMouseCursor(true);
}
void UInventoryComponent::CloseInventoryWidget()
{
	APlayerController* PlayerController = Cast<APlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!PlayerController || !Character) return;
	InventoryWidget->SetVisibility(ESlateVisibility::Hidden);
	FInputModeGameOnly InputGameMode;
	PlayerController->SetInputMode(InputGameMode);
	PlayerController->SetShowMouseCursor(false);
	PlayerController->SetIgnoreLookInput(false);
	PlayerController->SetIgnoreMoveInput(false);
	Character->SetGameAndUIState();
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}
