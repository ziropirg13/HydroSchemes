// HydroSchemes Simulator. All rights reserved.


#include "HSActors/BoardSchemeActor.h"
#include "HSActors/BoardPart.h"
#include "Player/HUBaseCharacter.h"
#include "Interfaces/InteractableInterface.h"
#include "HSActors/Board.h"
#include "HSActors/HydraulicPump.h"
#include "Utils/SchemeUtil.h"
#include "Utils/HoverUtils.h"

#define INTERACT ECC_GameTraceChannel1
#define BOARD_ACTOR_CHANNEL ECC_GameTraceChannel3

bool ABoardSchemeActor::IsCurrentSchemeEmpty() const
{
	return !(SocketRelationsSchemes.Num() && CurrentScheme.SocketRelationsScheme.Num());
}

const FSocketRelations* ABoardSchemeActor::GetCurrentSocketRelations(FString SocketName) const
{
	return CurrentScheme.SocketRelationsScheme.Find(SocketName);
}

ABoardSchemeActor::ABoardSchemeActor()
{
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	StaticMeshComponent->SetupAttachment(RootComponent);
	StaticMeshComponent->SetCollisionResponseToChannel(INTERACT, ECollisionResponse::ECR_Block);
}

void ABoardSchemeActor::InitSize(float CellSize)
{
	TArray<UStaticMeshComponent*> MeshComponents;
	GetComponents<UStaticMeshComponent>(MeshComponents);

	FBox CombinedBounds = FBox();
	CombinedBounds = CombinedBounds.MoveTo(GetActorLocation());

	for (UStaticMeshComponent* MeshComponent : MeshComponents) {
		if (MeshComponent) {
			FBox ComponentBounds = MeshComponent->Bounds.GetBox();
			CombinedBounds += ComponentBounds;
		}
	}
	FVector ActorBounds = CombinedBounds.GetSize();
	WidthInCell = FMath::CeilToInt(ActorBounds.Y / CellSize);
	HeightInCell = FMath::CeilToInt(ActorBounds.X / CellSize);
}

void ABoardSchemeActor::SetResponseToChannel(ECollisionChannel CollisionChannel, ECollisionResponse CollisionResponse)
{
	StaticMeshComponent->SetCollisionResponseToChannel(CollisionChannel, CollisionResponse);
}

void ABoardSchemeActor::SetRCT()
{
	StaticMeshComponent->SetRenderCustomDepth(true);
}

void ABoardSchemeActor::ShowHolo(AHUBaseCharacter* Character, const FHitResult HitResult)
{
	auto BoardPart = Cast<ABoardPart>(HitResult.GetActor());
	if (!Character->Holo || Character->Holo->IsActorBeingDestroyed()) {
		Character->Holo = CreateHolo(this, Character->HoloMaterial);
		if (Character->Holo) {
			Character->Holo->AttachToActor(BoardPart, FAttachmentTransformRules::SnapToTargetNotIncludingScale, BoardPart->SlotSocketName);
			Character->SetCorrectHoloState();
			Character->CheckHoloState(BoardPart);
		}
	}
	if (Character->Holo && !Character->Holo->IsActorBeingDestroyed()) {
		AActor* AttachActor = Character->Holo->GetAttachParentActor();
		if ((!AttachActor) || (AttachActor != BoardPart)) {
			if (AttachActor) Character->Holo->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
			Character->Holo->AttachToActor(BoardPart, FAttachmentTransformRules::SnapToTargetNotIncludingScale, BoardPart->SlotSocketName);
			Character->CheckHoloState(BoardPart);
		}
		else {
			Character->CheckHoloState(BoardPart);
		}
	}
}

ASchemeActor* ABoardSchemeActor::CreateHolo(ASchemeActor* PlacingElement, UMaterialInterface* HoloMaterial)
{
	if (!GetWorld()) return nullptr;
	auto Holo = GetWorld()->SpawnActor<ABoardSchemeActor>(PlacingElement->GetClass());
	if (!Holo) return nullptr;
	Holo->SetResponseToChannel(INTERACT, ECR_Ignore);
	Holo->SetRCT();
	TWeakObjectPtr<UMaterialInstanceDynamic> HoloDynMatInst = UMaterialInstanceDynamic::Create(HoloMaterial, Holo);
	Holo->SetHoloMaterialInstance(HoloDynMatInst);
	return Holo;
}

TArray<FName> ABoardSchemeActor::GetAllSocketsNames()
{
	TArray<FName> SocketOutputsArr;
	for (const TPair<FString, FBoardActorOutput>& PairSocket : SocketOutputs)
	{
		SocketOutputsArr.Add(PairSocket.Value.OutputName);
	}
	return SocketOutputsArr;
}

FVector ABoardSchemeActor::GetSocketLocationByName(FName SocketName)
{
	return StaticMeshComponent->GetSocketLocation(SocketName);
}

FName ABoardSchemeActor::GetNearestSocketName(FVector ImpactPoint, float MaxDistance)
{
	FName NearestSocketName;
	float NearestSocketDistance = MaxDistance;
	TArray<FName> SocketNames = GetAllSocketsNames();
	for (const FName& SocketName : SocketNames) {
		FVector SocketLocation = GetSocketLocationByName(SocketName);
		float Distance = FVector::Distance(ImpactPoint, SocketLocation);

		if (Distance < NearestSocketDistance) {
			NearestSocketDistance = Distance;
			NearestSocketName = SocketName;
		}
	}
	return NearestSocketName;
}

UStaticMeshComponent* ABoardSchemeActor::GetCableSocketComponent(const FName& SocketName) const
{

	if (StaticMeshComponent->DoesSocketExist(SocketName)) {
		return StaticMeshComponent;
	}
	return nullptr;
}

bool ABoardSchemeActor::CanPlaceHolo(FName SocketName)
{
	for (const TPair<FString, FBoardActorOutput>& Pair : SocketOutputs) {
		if (Pair.Value.OutputName == SocketName) {
			if (Pair.Value.bIsEmployed) return false;
			break;
		}
	}
	return true;
}

bool ABoardSchemeActor::IsSocketOutputExist(FName SocketName)
{
	if (SocketName == NAME_None) return false;
	for (const TPair<FString, FBoardActorOutput>& SocketOutput : SocketOutputs) {
		if (SocketOutput.Value.OutputName == SocketName) return true;
	}
	return false;
}

bool ABoardSchemeActor::IsSocketOutputEmployed(FName SocketName)
{
	for (const TPair<FString, FBoardActorOutput>& SocketOutput : SocketOutputs) {
		if (SocketOutput.Value.OutputName == SocketName) {
			return SocketOutput.Value.bIsEmployed;
		}
	}
	return false;
}


void ABoardSchemeActor::Clone()
{
	auto CloneElement = GetWorld()->SpawnActor<ABoardSchemeActor>(GetClass());
	if (!CloneElement) return;
	auto AttachBoardPart = Cast<ABoardPart>(GetAttachParentActor());
	CloneElement->AttachToActor(AttachBoardPart, FAttachmentTransformRules::SnapToTargetNotIncludingScale, AttachBoardPart->SlotSocketName);
	CloneElement->SetResponseToChannel(BOARD_ACTOR_CHANNEL, ECR_Block);
	auto Board = Cast<ABoard>(AttachBoardPart->GetAttachParentActor());
	if (!Board) return;
	int32 OutRow = -1;
	int32 OutColumn = -1;
	if (Board->CanPlaceHolo(CloneElement, OutRow, OutColumn) && OutRow != -1 && OutColumn != -1) {
		CloneElement->InitSize(Board->GetBoardPartSize().X);
		Board->EmployBoardParts(OutRow, OutColumn, CloneElement->GetWidth(), CloneElement->GetHeight());
	}
	CloneElement->SetBoardInteractState();
}

FOnBeginSocketViewOverlap& ABoardSchemeActor::GetOnBeginSocketViewOverlap()
{
	return HintComponent->OnBeginSocketViewOverlap;
}

FOnEndSocketViewOverlap& ABoardSchemeActor::GetOnEndSocketViewOverlap()
{
	return HintComponent->OnEndSocketViewOverlap;
}

void ABoardSchemeActor::PlaceElement()
{
	Clone();
	
}

void ABoardSchemeActor::EmploySocketOutputByName(FName SocketName)
{
	for (TPair<FString, FBoardActorOutput>& SocketOutput : SocketOutputs) {
		if (SocketOutput.Value.OutputName == SocketName) {
			SocketOutput.Value.bIsEmployed = true;
			return;
		}
	}
}

void ABoardSchemeActor::ReleaseSocketOutputByName(FName SocketName)
{
	for (TPair<FString, FBoardActorOutput>& SocketOutput : SocketOutputs) {
		if (SocketOutput.Value.OutputName == SocketName) {
			SocketOutput.Value.bIsEmployed = false;
			return;
		}
	}
}

void ABoardSchemeActor::SetOutputPressure(FString SocketName, float Pressure)
{
	UE_LOG(LogTemp, Warning, TEXT("Start SetOutputPressure for %s"), *GetName());
	UE_LOG(LogTemp, Warning, TEXT("SocketName - %s"), *SocketName);
	UE_LOG(LogTemp, Warning, TEXT("Pressure - %f"), Pressure);
	auto Socket = SocketOutputs.Find(SocketName);
	Socket->Pressure = Pressure;
	Socket->bOutMode = true;
	OnSetOutputPressureAfter(SocketName, Pressure);
	//if (Socket->RelatedActorData.RelatedActor && !Socket->RelatedActorData.RelatedActorSocket.IsEmpty()) {
	//	Socket->RelatedActorData.RelatedActor->SetInputPressure(Socket->RelatedActorData.RelatedActorSocket, Pressure);
	//}
}

void ABoardSchemeActor::SetInputPressure(FString SocketName, float Pressure)
{
	UE_LOG(LogTemp, Warning, TEXT("Start SetInputPressure for %s"), *GetName());
	UE_LOG(LogTemp, Warning, TEXT("SocketName - %s"), *SocketName);
	UE_LOG(LogTemp, Warning, TEXT("Pressure - %f"), Pressure);
	auto Socket = SocketOutputs.Find(SocketName);
	Socket->Pressure = Pressure;
	Socket->bOutMode = false;
	OnSetInputPressureAfter(SocketName, Pressure);
	UE_LOG(LogTemp, Warning, TEXT("End SetInputPressure for %s"), *GetName());
}

void ABoardSchemeActor::OnSetOutputPressureAfter(FString SocketName, float Pressure)
{
}

void ABoardSchemeActor::OnSetInputPressureAfter(FString SocketName, float Pressure)
{
}

void ABoardSchemeActor::SetSocketRelatedActor(FString ThisSocketName, ABoardSchemeActor* RelatedActor, FString RelatedSocketName)
{
	FBoardActorOutput* Socket = SocketOutputs.Find(ThisSocketName);
	Socket->RelatedActorData.RelatedActor = RelatedActor;
	Socket->RelatedActorData.RelatedActorSocket = RelatedSocketName;
}

void ABoardSchemeActor::CheckPressure()
{
	// in > 0 - ������ �� ������
	// out > 0 - ����� ������ �������� �������� ��� �����������
	// in = 0 - ������ �� ������
	// out = 0 - ������ �� ������

	// in = 0 in = 0 - ��������� bfs ��� ��� ��� ����� ���� ����� �������
	// in > 0 in > 0 - ������ �� ������ 
	// in > 0 in = 0 - ��������� bfs � 1 in
	// out > 0 out > 0 - ����� �������� ������� ��� �������
	// out > 0 out = 0 - ����� �������� ��������
	// in > 0 out > 0 - ������ �� ������
	// in > 0 out = 0 - bfs ��� in
	// in = 0 out > 0 - bfs ��� out
	// in = 0 out = 0 - ������ �� ������

	TSet<FString> Visited;
	UE_LOG(LogTemp, Warning, TEXT("Start CheckPressure for %s"), *GetName());
	UE_LOG(LogTemp, Warning, TEXT("CurrentScheme - %s"), *CurrentScheme.ToString());
	for (TPair<FString, FSocketRelations> SocketRelation : CurrentScheme.SocketRelationsScheme) {
		UE_LOG(LogTemp, Warning, TEXT("Iteration: SocketRelation: Key - %s, Value - %s"), *SocketRelation.Key, *SocketRelation.Value.ToString());
		if (Visited.Contains(SocketRelation.Key)) continue;
		Visited.Add(SocketRelation.Key);
		
		FBoardActorOutput* FirstSocketOutput = SocketOutputs.Find(SocketRelation.Key);
		if (!FirstSocketOutput) continue;
		for (FString SocketName : SocketRelation.Value.SocketRelations) {
			UE_LOG(LogTemp, Warning, TEXT("Subiteration - %s"), *SocketName);
			Visited.Add(SocketName);
			FBoardActorOutput* SecondSocketOutput = SocketOutputs.Find(SocketName);
			UE_LOG(LogTemp, Warning, TEXT("FirstSocketOutput: Mode - %i, Pressure - %f; SecondSocketOutput: Mode - %i, Pressure - %f"), FirstSocketOutput->bOutMode, FirstSocketOutput->Pressure, SecondSocketOutput->bOutMode, SecondSocketOutput->Pressure);
			// in = 0 in = 0
			if ((!FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure == 0.0f) && (!SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure == 0.0f)) {
				SetOutputPressure(SocketRelation.Key, 0.f);
				SchemeUtil::ScanScheme(this, SocketRelation.Key);
				if (FirstSocketOutput->Pressure == 0.f) {
					SetOutputPressure(SocketName, 0.f);
					SchemeUtil::ScanScheme(this, SocketName);
				}
				continue;
			}
			// in > 0 in = 0
			if ((!FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure > 0.f) && (!SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure == 0.f)) {
				SchemeUtil::ScanScheme(this, SocketRelation.Key);
				continue;
			}
			if ((!FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure == 0.f) && (!SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure > 0.f)) {
				SchemeUtil::ScanScheme(this, SocketName);
				continue;
			}

			//out > 0 out > 0
			if ((FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure > 0.f) && (SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure > 0.f)) {
				SetOutputPressure(SocketRelation.Key, 0.f);
				SchemeUtil::ScanScheme(this, SocketRelation.Key);
				if (FirstSocketOutput->Pressure == 0.f) {
					SetOutputPressure(SocketName, 0.f);
					SchemeUtil::ScanScheme(this, SocketName);
				}
				continue;
			}

			//out > 0 out = 0
			if ((FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure > 0.f) && (SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure == 0.f)) {
				SetOutputPressure(SocketRelation.Key, 0.f);
				SchemeUtil::ScanScheme(this, SocketRelation.Key);
				continue;
			}
			if ((FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure == 0.f) && (SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure > 0.f)) {
				SetOutputPressure(SocketName, 0.f);
				SchemeUtil::ScanScheme(this, SocketName);
				continue;
			}

			//in > 0 out = 0
			if ((!FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure > 0.f) && (SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure == 0.f)) {
				SchemeUtil::ScanScheme(this, SocketRelation.Key);
				continue;
			}

			if ((FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure == 0.f) && (!SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure > 0.f)) {
				SchemeUtil::ScanScheme(this, SocketName);
				continue;
			}

			//in = 0 out > 0
			if ((!FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure == 0.f) && (SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure > 0.f)) {
				SetOutputPressure(SocketName, 0.f);
				SchemeUtil::ScanScheme(this, SocketName);
				continue;
			}

			if ((FirstSocketOutput->bOutMode && FirstSocketOutput->Pressure > 0.f) && (!SecondSocketOutput->bOutMode && SecondSocketOutput->Pressure == 0.f)) {
				SetOutputPressure(SocketRelation.Key, 0.f);
				SchemeUtil::ScanScheme(this, SocketRelation.Key);
				continue;
			}
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("Single CheckPressure for %s"), *GetName());
	for (TPair<FString, FBoardActorOutput>& SocketOutput : SocketOutputs) {
		UE_LOG(LogTemp, Warning, TEXT("Single Iterate: SocketOutput - %s: %s"), *SocketOutput.Key, *SocketOutput.Value.ToString());
		if (Visited.Contains(SocketOutput.Key)) continue;
		if (SocketOutput.Value.bOutMode && SocketOutput.Value.Pressure > 0.f) {
			SetOutputPressure(SocketOutput.Key, 0.f);
			SchemeUtil::ScanScheme(this, SocketOutput.Key);
		}
	}
	UE_LOG(LogTemp, Warning, TEXT("End CheckPressure for %s"), *GetName());
}

void ABoardSchemeActor::SocketBroadcast()
{
	for (TPair<FString, FBoardActorOutput>& Socket : SocketOutputs) {
		Socket.Value.Delegate.Broadcast();
	}
}

FString ABoardSchemeActor::PrintSocketOutputs()
{
	FString Result;
	for (TPair<FString, FBoardActorOutput> SocketOutput : SocketOutputs) {
		Result += SocketOutput.Key + ": {\n" + SocketOutput.Value.ToString() + "}\n";
	}
	return Result;
}


FString FRelatedActorData::ToString() {
	FString Result;
	Result += "RelatedActor: ";
	Result += RelatedActor ? RelatedActor->GetName() : "null";
	Result += "\n";
	Result += "RelatedActorSocket: " + RelatedActorSocket;
	return Result;
}

void ABoardSchemeActor::BeginPlay()
{
	Super::BeginPlay(); 

	GetOnBeginSocketViewOverlap().AddUObject(this, &ABoardSchemeActor::ShowHintSocketWidget);
	GetOnEndSocketViewOverlap().AddUObject(this, &ABoardSchemeActor::HideHintSocketWidget);
}

void ABoardSchemeActor::ShowHintSocketWidget() 
{
	UE_LOG(LogTemp, Warning, TEXT("ShowHintSocketWidget")); 
	UE_LOG(LogTemp, Warning, TEXT("OldSocket - %s"), *OldSocket.ToString());
	HintComponent->ShowHintSocketWidget(OldSocket); 
}

void ABoardSchemeActor::HideHintSocketWidget()
{ 
	UE_LOG(LogTemp, Warning, TEXT("HideHintocketWidget"));
	HintComponent->HideHintocketWidget(); 
}
void ABoardSchemeActor::ShowHint()
{
	Super::ShowHint();
	// �������� ������ OnSocketHover � ������� OnHover
	//ABoardSchemeActor* BoardSchemeActor = Cast<ABoardSchemeActor>(GetOwner());
	//if (!BoardSchemeActor) return;
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	UE_LOG(LogTemp, Warning, TEXT("Link"));
	HoverHitDelegateHandle = Character->OnHoverHit.AddUObject(this, &ABoardSchemeActor::OnSocketHover);
	HideHintSocketWidget();
	OldSocket = FName();
}

void ABoardSchemeActor::HideHint()
{
	Super::HideHint();
	// ������� ������ OnSocketHover �� ������� OnHoverHit
	//ABoardSchemeActor* BoardSchemeActor = Cast<ABoardSchemeActor>(GetOwner());
	//if (!BoardSchemeActor) return;
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	if (HoverHitDelegateHandle.IsValid())
	{
		UE_LOG(LogTemp, Warning, TEXT("Unlink"));
		Character->OnHoverHit.Remove(HoverHitDelegateHandle);
	}
	HoverHitDelegateHandle.Reset();
}

FBoardActorOutput* ABoardSchemeActor::GetSocketInfo(FString SocketName)
{
	return SocketOutputs.Find(SocketName);
}

void ABoardSchemeActor::OnSocketHover(FHitResult HitResult)
{
	HoverUtils::Hover(this, OldSocket, HitResult);
}