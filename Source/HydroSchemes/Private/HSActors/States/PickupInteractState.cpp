// HydroSchemes Simulator. All rights reserved.


#include "HSActors/States/PickupInteractState.h"
#include "Interfaces/PickUpInterface.h"
#include "Inventory/InventoryElement.h"
#include "Kismet/GameplayStatics.h"
#include "Player/HUBaseCharacter.h"

void UPickupInteractState::Interact(UObject* StateOwner)
{
	if (StateOwner && StateOwner->Implements<UPickUpInterface>()) {
		IPickUpInterface* PickupableActor = Cast<IPickUpInterface>(StateOwner);
		Pickup(PickupableActor);
	}
}

void UPickupInteractState::Pickup(IPickUpInterface* StateOwner)
{
	AHUBaseCharacter* Character = Cast<AHUBaseCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (!Character) return;
	auto InventoryElement = FInventoryElement();
	StateOwner->PickUpItem(&InventoryElement);
	Character->AddItemToInventory(InventoryElement);
}
